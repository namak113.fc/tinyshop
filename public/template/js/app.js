$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function htmlMsg(msg = '', type = 'success') {
    return `<div class="alert alert-${type}">${msg}</div>`
}

function editCurrency(id) {
    let code = $(`#data-${id} .code`).text();
    let price = $(`#data-${id} .price`).text();
    let description = $(`#data-${id} .description`).text();

    $(`#data-${id} .code, #data-${id} .price, #data-${id} .description`).attr('contenteditable', '').css('background-color', '#CCFFFF');
    $(`#data-${id} .op`).html(`
    <a href="javascript:void(0)" onclick="updateCurrency(${id})"><i class="fas fa-save"></i></a>
    <a href="javascript:void(0)" id="close-edit-customer-${id}" onclick="closeEditCurrency(${id}, '${code}', '${price}', '${description}')"><i class="fas fa-window-close text-danger"></i></a>
    `);
}

function closeEditCurrency(id, code, price, description) {
    $(`#data-${id} .code, #data-${id} .price, #data-${id} .description`).attr('contenteditable', false).css('background-color', '');
    $(`#data-${id} .op`).html(`<a href="#" onclick="removeRow('${id}')"><i class="fas fa-trash-alt text-danger"></i></a>
    <a href="javascript:void(0)" onclick="editCurrency(${id})"><i class="fas fa-pen text-info"></i></a>`);

    //reset data
    $(`#data-${id} .code`).text(code);
    $(`#data-${id} .price`).text(price);
    $(`#data-${id} .description`).text(description);
}

function updateCurrency(id) {
    let code = $(`#data-${id} .code`).text();
    let price = $(`#data-${id} .price`).text();
    let description = $(`#data-${id} .description`).text();

    $('.alert').remove();

    $.ajax({
        type: "put",
        url: "/admin/currency/" + id,
        data: {code, price, description},
        dataType: "JSON",
        success: function (response) {
            $('.alert').remove();
            if (response.success) {
                $('#table-data').before(htmlMsg(response.message));
                closeEditCurrency(id, code, price, description);
            } else {  
                $('#table-data').before(htmlMsg(response.message, 'danger'));
                $('#close-edit-customer-' + id).trigger('click');
            }
        },
        error: function (response) {
            var errors = response.responseJSON.errors;
            for (const key in errors) {
                var msg = '';
                for (const value of errors[key]) {
                    msg += value + ' ';
                }

                $(`#data-${id} .${key}`).css('background-color', '#DC3545').attr('title', msg);
                $('#table-data').before(htmlMsg('Thông tin nhập không hợp lệ', 'danger'));
            }
        }
    });
}

function removeRow(id) {
    if (confirm('Bạn có chắc muốn xóa?')) {
        $.ajax({
            type: "delete",
            url: "/admin/currency/" + id,
            dataType: "JSON",
            success: function (response) {
                location.reload();
            }
        });
    }
}
