<?php

use App\Http\Controllers\trangChuController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [trangChuController::class, 'index']);

Route::group(['prefix' => '/admin'], function() {

    Route::group(['prefix' => '/danh-muc'], function() {
        Route::get('/index', [\App\Http\Controllers\DanhMucController::class, 'index']);
        Route::get('/get-data', [\App\Http\Controllers\DanhMucController::class, 'getData']);
        Route::get('/update-status/{id}',  [\App\Http\Controllers\DanhMucController::class, 'updateStatus']);
        Route::post('/index', [\App\Http\Controllers\DanhMucController::class, 'store']);
        Route::get('/edit/{id}', [\App\Http\Controllers\DanhMucController::class, 'edit']);
        Route::post('/update', [\App\Http\Controllers\DanhMucController::class, 'update']);
        Route::get('/destroy/{id}', [\App\Http\Controllers\DanhMucController::class, 'destroy']);
    });

    Route::group(['prefix' => '/san-pham'], function() {
        Route::post('/search', [\App\Http\Controllers\ChiTietNhapKhoController::class, 'search']);
        Route::get('/index', [\App\Http\Controllers\SanPhamController::class, 'index']);
        Route::get('/data', [\App\Http\Controllers\SanPhamController::class, 'getData']);
        Route::post('/index', [\App\Http\Controllers\SanPhamController::class, 'store']);
        Route::post('/check-product-id', [\App\Http\Controllers\SanPhamController::class, 'checkProuctId']);
        Route::get('/auto-complete', [\App\Http\Controllers\SanPhamController::class, 'autoComplete']);
    });




    Route::get('/login',[\App\Http\Controllers\trangchuController::class,'login'])->name('login');
    Route::post('/login-admin-action',[\App\Http\Controllers\trangchuController::class,'loginAction']);
    Route::get('/register',[\App\Http\Controllers\trangchuController::class,'register'])->name('register');
    Route::post('/register',[\App\Http\Controllers\trangchuController::class,'store']);
    Route::get('/logout',[\App\Http\Controllers\trangchuController::class,'logout'])->name('logout');


    Route::get('main', function () {
        return view('admin.index', ['title' => 'Trang quản trị']);
    })->name('admin.main');

    Route::get('currency',[\App\Http\Controllers\CurrencyController::class,'index'])->name('currency');
    Route::put('currency/{currency}',[\App\Http\Controllers\CurrencyController::class,'update']);
    Route::get('currency/create',[\App\Http\Controllers\CurrencyController::class,'create']);
    Route::post('currency/create',[\App\Http\Controllers\CurrencyController::class,'store']);
    Route::delete('currency/{currency}',[\App\Http\Controllers\CurrencyController::class,'destroy']);
});
